# Logstash

Some explanation about the customization of this image.

## Elasticsearch configuration

The docker etnrypoint was changed to be able to configure Elasticsearch before Logstash is started (and therefore before the first data are sent to Elastic).

This includes:

- an ILM policy creation
- an index template creation (which associates the index to the policy)


## Memory configuration

The logstash image was tweaked to be able to set the heap size via the LS_JAVA_OPTS env var. Explanation:

- the logstash image comes with a `jvm.options` file in which the Xms and Xmx are already set,
  therefore the Xms and Xmx options that would be passed in the LS_JAVA_OPTS wouldn't be taken into account.
- therefore we chose to tweak the `Dockerfile` to build an image with the Xms and Xmx options commented

See: issue [Xms and Xmx specified in ES_JAVA_OPTS variable are not taken into account, overloaded by jvm.options definition](https://github.com/elastic/elasticsearch-docker/issues/22) which applies also to LS_JAVA_OPTS
