select
    ql.id as id,
    cast(time as timestamp) as queuetime,
    ql.callid,
    ql.queuename,
    qf.displayname as queuedisplayname,
    ql.agent,
    af.number as agentnumber,
    af.firstname || ' ' || af.lastname as agentname,
    ql.event,
    ql.data1,
    ql.data2,
    ql.data3,
    ql.data4,
    ql.data5,
    ag.name as groupname
from
    queue_log as ql 
    left outer join queuefeatures as qf 
        on (ql.queuename = qf.name)
    left outer join agentfeatures as af
        on (ql.agent = 'Agent/' || af.number)
    left outer join agentgroup as ag
        on (af.numgroup = ag.id)
where
    ql.id > :sql_last_value
order by ql.id asc