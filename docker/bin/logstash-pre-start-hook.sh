#!/bin/bash -e

# Configures Elasticsearch:
#   - creates ILM policy
#   - and Index template in Elasticsearch
#   (Needs Elasticsearch URL as first args)
# Initializes first ID of queue_log for jdbc input plugin
# (Can be cusomized with the LS_QUEUELOG_INIT_NB_DAYS).

ES_URL=$1
CURL_CMD="curl -s -o /dev/null"
DB_CNX="postgresql://stats:stats@pgxivocc:5432/xivo_stats"

LS_QUEUELOG_LAST_RUN_FILE="/usr/share/logstash/.logstash_jdbc_last_run" # This is the default value
re_is_numeric='^[0-9]+$'
if ! [[ $LS_QUEUELOG_INIT_NB_DAYS =~ $re_is_numeric ]] ; then
  LS_QUEUELOG_INIT_NB_DAYS=7
fi

# Create ILM policy
if [ $(${CURL_CMD} -w "%{http_code}" -X GET "${ES_URL}/_ilm/policy/xivo_policy") -eq "404" ]; then
  echo
  echo "Creating ILM policy in Elasticsearch"
  echo

  ${CURL_CMD} -X PUT "${ES_URL}/_ilm/policy/xivo_policy?pretty" -H 'Content-Type: application/json' -d @/etc/logstash/elastic-xivo_policy.json
fi

# Create Index Template
if [ $(${CURL_CMD} -w "%{http_code}" -I "${ES_URL}/_template/queuelog_template") -eq "404" ]; then
  echo
  echo "Creating Index Template in Elasticsearch"
  echo

  ${CURL_CMD} -X PUT "${ES_URL}/_template/queuelog_template?pretty" -H 'Content-Type: application/json' -d @/etc/logstash/elastic-queuelog_template.json
fi

# Initiate sql_last_value if file does not exist
if [ ! -e ${LS_QUEUELOG_LAST_RUN_FILE} ]; then
  echo
  echo "Initializing queue_log last value to take ${LS_QUEUELOG_INIT_NB_DAYS} days of history"
  echo

  LS_QUEUELOG_LAST_ID=$(psql -tq --dbname=${DB_CNX} -c "select min(id) from queue_log where cast(time as timestamp) > (select now() - interval '${LS_QUEUELOG_INIT_NB_DAYS} day')")
  echo "--- ${LS_QUEUELOG_LAST_ID}" > ${LS_QUEUELOG_LAST_RUN_FILE}
fi

exit 0